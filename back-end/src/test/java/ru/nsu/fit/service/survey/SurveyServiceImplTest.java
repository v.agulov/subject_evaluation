package ru.nsu.fit.service.survey;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.nsu.fit.AbstractContextualTest;
import ru.nsu.fit.entity.Survey;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RequiredArgsConstructor
@DisplayName("Получение курса по идентификатору")
class SurveyServiceImplTest extends AbstractContextualTest {
    @Autowired
    private SurveyService surveyService;

    @Test
    @DatabaseSetup(value = {"/service/survey/setup.xml"}, type = DatabaseOperation.INSERT)
    void testGetSurveysForStartNotificationSending() {
        List<Survey> surveys = surveyService.getSurveysForStartNotificationSending();
        assertTrue(CollectionUtils.isNotEmpty(surveys));
        assertThat(surveys, hasSize(1));
    }

    @Test
    @DatabaseSetup(value = {"/service/survey/setup.xml"}, type = DatabaseOperation.INSERT)
    void testGetSurveysForEndNotificationSending() {
        List<Survey> surveys = surveyService.getSurveysForEndNotificationSending();
        assertTrue(CollectionUtils.isNotEmpty(surveys));
        assertThat(surveys, hasSize(1));
    }
}