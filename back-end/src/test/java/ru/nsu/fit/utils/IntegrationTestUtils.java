package ru.nsu.fit.utils;

import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class IntegrationTestUtils {
    private IntegrationTestUtils() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    public static ResultMatcher jsonContent(String path) {
        return MockMvcResultMatchers.content().json(extractFileContent(path), true);
    }

    @Nonnull
    public static String extractFileContent(String relativePath) {
        try (InputStream systemResourceAsStream = getSystemResourceAsStream(relativePath)) {
            if (systemResourceAsStream == null) {
                throw new RuntimeException("Error during reading from file " + relativePath);
            }
            return IOUtils.toString(systemResourceAsStream, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException("Error during reading from file " + relativePath, e);
        }
    }
}
