package ru.nsu.fit.configuration;


import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;

@Configuration
@Import({
    RepositoryConfiguration.class,
    DatasourceTestConfiguration.class,
    LiquibaseConfiguration.class,
    ClockConfiguration.class,
    WebSecurityTestConfiguration.class,
    CleanDatabaseTestConfiguration.class,
})
@MockBean({
    JavaMailSender.class
})
public class IntegrationTestConfiguration {
}
