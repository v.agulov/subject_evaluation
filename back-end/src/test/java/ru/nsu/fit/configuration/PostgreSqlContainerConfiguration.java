package ru.nsu.fit.configuration;

import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.PostgreSQLContainer;

public class PostgreSqlContainerConfiguration {
    @Bean
    public PostgreSQLContainer postgreSQLContainer() {
        PostgreSQLContainer container = new PostgreSQLContainer("postgres:10.7");
        container.start();
        return container;
    }
}
