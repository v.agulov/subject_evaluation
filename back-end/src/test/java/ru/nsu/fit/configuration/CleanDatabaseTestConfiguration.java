package ru.nsu.fit.configuration;

import javax.persistence.EntityManager;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.nsu.fit.utils.DatabaseCleaner;
import ru.nsu.fit.utils.DatabaseCleanerImpl;

@Configuration
public class CleanDatabaseTestConfiguration {
    @Bean
    public DatabaseCleaner databaseCleaner(EntityManager entityManager) {
        return new DatabaseCleanerImpl(entityManager);
    }
}
