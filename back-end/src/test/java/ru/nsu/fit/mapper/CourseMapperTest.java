package ru.nsu.fit.mapper;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import ru.nsu.fit.dto.course.CourseResponseDto;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

import java.util.List;

@ExtendWith(SoftAssertionsExtension.class)
public class CourseMapperTest {
    private CourseMapper courseMapper = Mappers.getMapper(CourseMapper.class);

    private static final List<Course> COURSES = List.of(
        createCourse(1, "Calculus", "desc1", CourseStatus.ACTIVE, CourseLevel.MAGISTRACY),
        createCourse(2, "PE", "desc2", CourseStatus.ACTIVE, CourseLevel.POSTGRADUATE)
    );

    @Test
    public void testForNull(SoftAssertions softly) {
        softly.assertThat(courseMapper.courseToDto(null)).isNull();
    }

    @Test
    public void courseToDto(SoftAssertions softly) {
        Course course = COURSES.get(0);
        CourseResponseDto courseResponseDto = courseMapper.courseToDto(course);
        softly.assertThat(courseResponseDto).isNotNull();
        softly.assertThat(courseResponseDto)
            .usingRecursiveComparison()
            .isEqualTo(course);
    }

    @Test
    public void testCoursesToDtos(SoftAssertions softly) {
        Page<Course> page = new PageImpl<>(COURSES);
        Page<CourseResponseDto> dtosPage = courseMapper.coursesToDtos(page);
        softly.assertThat(dtosPage).isNotNull();
        List<CourseResponseDto> courseResponseDtos = dtosPage.getContent();
        softly.assertThat(courseResponseDtos)
            .usingRecursiveComparison()
            .isEqualTo(COURSES);
    }

    private static Course createCourse(int id,
                                       String name,
                                       String description,
                                       CourseStatus status,
                                       CourseLevel level) {
        Course course = new Course();
        course.setId(id);
        course.setName(name);
        course.setDescription(description);
        course.setStatus(status);
        course.setLevel(level);
        return course;
    }
}
