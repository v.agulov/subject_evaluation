package ru.nsu.fit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.AbstractContextualTest;
import ru.nsu.fit.dto.survey.AnswerChoiceRequestDto;
import ru.nsu.fit.dto.survey.QuestionRequestDto;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.nsu.fit.util.paths.SurveyPaths.SURVEY_ROOT_PATH;
import static ru.nsu.fit.utils.IntegrationTestUtils.jsonContent;

@DisplayName("Тесты опросов")
@DatabaseSetup(value = "/controller/survey/before/setup.xml")
public class SurveyControllerTest extends AbstractContextualTest {

    @Autowired
    protected ObjectMapper objectMapper;

    @Test
    @DisplayName("Сохранение курса")
    @ExpectedDatabase(
        value = "/controller/survey/after/create_survey.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void createCourse() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(SURVEY_ROOT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createSurveyRequestDto()))
        )
            .andExpect(status().isOk())
            .andExpect(jsonContent("controller/survey/response/create_survey.json"));
    }

    @ParameterizedTest
    @MethodSource("validateArguments")
    public void validate(
        String fieldName,
        String errorMessage,
        UnaryOperator<SurveyRequestDto> surveyModifier
    ) throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(SURVEY_ROOT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(surveyModifier.apply(createSurveyRequestDto())))
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("errorMessage").value("Validation error"))
            .andExpect(jsonPath("fieldErrors." + fieldName).value(errorMessage))
            .andDo(print());
    }

    @Nonnull
    private static Stream<Arguments> validateArguments() {
        return Stream.<Triple<String, String, UnaryOperator<SurveyRequestDto>>>of(
            Triple.of("authorId", "must not be null", s -> s.setAuthorId(null)),
            Triple.of("courseId", "must not be null", s -> s.setCourseId(null)),
            Triple.of("year", "Поле 'year' некорректно", s -> s.setYear("2020:2021")),
            Triple.of("semester", "Поле 'semester' некорректно", s-> s.setSemester(3))
        )
            .map(t -> Arguments.of(t.getLeft(), t.getMiddle(), t.getRight()));
    }

    @Nonnull
    private SurveyRequestDto createSurveyRequestDto() {
        return new SurveyRequestDto()
            .setAuthorId(1)
            .setCourseId(11)
            .setStartDate(LocalDate.parse("2021-11-25"))
            .setEndDate(LocalDate.parse("2021-11-30"))
            .setYear("2021 - 2022")
            .setSemester(1)
            .setStudentIds(List.of(2))
            .setTeacherIds(List.of(1))
            .setQuestions(List.of(
                new QuestionRequestDto()
                .setTitle("Расскажите общие впечатления от курса")
                .setType(QuestionType.OPEN),
                new QuestionRequestDto().setTitle("Оцените организацию курса")
                .setType(QuestionType.SINGLE_OPTION)
                .setAnswerChoices(List.of(
                    new AnswerChoiceRequestDto().setTitle("Хорошая"),
                    new AnswerChoiceRequestDto().setTitle("Плохая")
                ))
            ));
    }

    @Nonnull
    private String toJson(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }

}
