package ru.nsu.fit.controller.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.AbstractContextualTest;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Set;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.nsu.fit.util.paths.UserPaths.USERS_ROOT_PATH;

@RequiredArgsConstructor
@DisplayName("Тесты на контроллер пользователя")
@DatabaseSetup("/controller/user/before/setup.xml")
public class UserControllerTest extends AbstractContextualTest {
    private static final String BLOCK_USERS = USERS_ROOT_PATH + "/block";
    private static final String ACTIVATE_USERS = USERS_ROOT_PATH + "/activate";
    private static final int EXISTING_ID1 = 101;
    private static final int EXISTING_ID2 = 102;
    private static final int EXISTING_ID3 = 103;
    private static final int ABSENT_ID = 100;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Блокировка пользователей")
    @ExpectedDatabase(
        value = "/controller/user/after/block_users.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void blockUsers() throws Exception {
        Set<Integer> idUsers = Set.of(EXISTING_ID1, EXISTING_ID2);
        mockMvc.perform(
            MockMvcRequestBuilders.post(BLOCK_USERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idUsers))
        )
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Активирование пользователей")
    @ExpectedDatabase(
        value = "/controller/user/after/activate_users.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void activateUsers() throws Exception {
        Set<Integer> idUsers = Set.of(EXISTING_ID3);
        mockMvc.perform(
            MockMvcRequestBuilders.post(ACTIVATE_USERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idUsers))
        )
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Блокировка с несуществующим пользователем")
    @ExpectedDatabase(
        value = "/controller/user/after/block_not_exist_user.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void blockNotExistUsers() throws Exception {
        Set<Integer> idUsers = Set.of(EXISTING_ID1, ABSENT_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.post(BLOCK_USERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idUsers))
        )
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Активирование с несуществующим пользователем")
    @ExpectedDatabase(
        value = "/controller/user/after/activate_not_exist_user.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void activateNotExistUsers() throws Exception {
        Set<Integer> idUsers = Set.of(EXISTING_ID3, ABSENT_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.post(ACTIVATE_USERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idUsers))
        )
            .andExpect(status().isOk());
    }

    @Nonnull
    private String toJson(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }
}
