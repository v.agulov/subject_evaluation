package ru.nsu.fit.controller.file;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.AbstractContextualTest;
import ru.nsu.fit.dao.FileRepository;
import ru.nsu.fit.dto.file.FileCreateDto;
import ru.nsu.fit.dto.file.FileResponseDto;
import ru.nsu.fit.entity.File;
import ru.nsu.fit.entity.enums.FileType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RequiredArgsConstructor
@DisplayName("Загрузка и скачивание фалов")
public class FileControllerTest extends AbstractContextualTest {
    private static final String FILES_ROOT_PATH = "/api/v1/files/";
    private static final String GET_FILE = FILES_ROOT_PATH + "{id}";
    private static final String DOWNLOAD_FILE = FILES_ROOT_PATH + "{id}/download";
    private static final String SAVE_FILE = FILES_ROOT_PATH + "save";
    private static final String UPLOAD_FILE = FILES_ROOT_PATH + "upload";
    private static final int EXISTING_ID = 2;

    private static final List<FileResponseDto> FILES = List.of(
        new FileResponseDto(
            2,
            "filename2.txt",
            MediaType.TEXT_PLAIN_VALUE,
            FileType.COURSE_PROGRAM,
            "http://localhost/api/v1/files/2/download",
            "some text2".getBytes()
        ),
        new FileResponseDto(
            1,
            "filename.txt",
            MediaType.TEXT_PLAIN_VALUE,
            FileType.COURSE_PROGRAM,
            "http://localhost/api/v1/files/1/download",
            "some text1".getBytes()
        )
    );

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FileRepository fileRepository;

    @Test
    @DisplayName("Сохранение файла")
    public void uploadFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
            "file",
            "filename.txt",
            MediaType.TEXT_PLAIN_VALUE,
            "some text1".getBytes()
        );
        mockMvc.perform(MockMvcRequestBuilders.multipart(UPLOAD_FILE)
            .file(file)
            .param("fileType", String.valueOf(FileType.COURSE_PROGRAM)))
            .andExpect(content().json(toJson(FILES.get(1))))
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Сохранение файла курса")
    public void saveFileDto() throws Exception {
        FileCreateDto fileCreateDto = new FileCreateDto(
            "filename2.txt",
            MediaType.TEXT_PLAIN_VALUE,
            FileType.COURSE_PROGRAM,
            "",
            "some text2".getBytes()
        );
        mockMvc.perform(MockMvcRequestBuilders.post(SAVE_FILE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(toJson(fileCreateDto))
        )
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(content().json(String.valueOf(2)))
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Сохранение пустого файла")
    public void uploadEmptyFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
            "file",
            "filenameEmpty.txt",
            MediaType.TEXT_PLAIN_VALUE,
            "".getBytes()
        );
        mockMvc.perform(MockMvcRequestBuilders.multipart(UPLOAD_FILE)
            .file(file)
            .param("courseFileType", String.valueOf(FileType.COURSE_PROGRAM)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Успешное получение файла по идентификатору")
    public void downloadFile() throws Exception {
        fileRepository.save(
            new File(
                "filename.txt",
                "text/plain",
                FileType.COURSE_PROGRAM,
                "http://localhost/api/v1/files/1/download",
                "some text".getBytes()
            )
        );
        mockMvc.perform(MockMvcRequestBuilders.get(DOWNLOAD_FILE, EXISTING_ID))
            .andExpect(status().isOk())
            .andExpect(content().contentType(FILES.get(0).getMimeType()))
            .andExpect(content().bytes(FILES.get(0).getContent()));
    }

    @Test
    @DisplayName("Успешное получение файла курса по идентификатору")
    public void getFile() throws Exception {
        fileRepository.save(
            new File(
                "filename.txt",
                "text/plain",
                FileType.COURSE_PROGRAM,
                "http://localhost/api/v1/files/1/download",
                "some text1".getBytes()
            )
        );
        mockMvc.perform(MockMvcRequestBuilders.get(GET_FILE, 1L))
            .andExpect(status().isOk())
            .andExpect(content().json(toJson(FILES.get(1))));
    }

    @Nonnull
    private String toJson(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }
}
