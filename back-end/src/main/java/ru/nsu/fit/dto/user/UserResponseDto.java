package ru.nsu.fit.dto.user;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.enums.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UserResponseDto {

    private Integer id;

    private String email;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String groupNumber;

    private UserStatus status;

    private Set<Role> roles;
}
