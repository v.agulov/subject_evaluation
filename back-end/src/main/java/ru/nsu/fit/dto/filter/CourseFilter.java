package ru.nsu.fit.dto.filter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

import java.util.Set;

@Data
@Accessors(chain = true)
@ApiModel("Фильтр для поиска курсов")
public class CourseFilter {
    @ApiModelProperty(value = "Подстрока названия курса", example = "Математический")
    private String name;

    @ApiModelProperty(value = "Статус курса", example = "ACTIVE")
    private Set<CourseStatus> statuses;

    @ApiModelProperty(value = "Уровень курса", example = "UNDERGRADUATE")
    private Set<CourseLevel> levels;
}
