package ru.nsu.fit.dto.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.entity.enums.Role;

@Documented
@Constraint(validatedBy = ValidStudentGroupNumber.StudentGroupNumberValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidStudentGroupNumber {

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class StudentGroupNumberValidator
        implements ConstraintValidator<ValidStudentGroupNumber, UserRequestDto> {

        @Override
        public boolean isValid(UserRequestDto userRequestDto, ConstraintValidatorContext context) {
            Set<Role> roles = userRequestDto.getRoles();
            if (roles == null) {
                return true;
            }
            boolean isStudent = roles.contains(Role.STUDENT);
            boolean groupNumberSpecified = userRequestDto.getGroupNumber() != null;
            if (isStudent && groupNumberSpecified || !isStudent && !groupNumberSpecified) {
                return true;
            }

            context.disableDefaultConstraintViolation();
            if (isStudent) {
                context
                    .buildConstraintViolationWithTemplate("{student.group-number.must-not-be-null}")
                    .addPropertyNode("groupNumber")
                    .addConstraintViolation();
                return false;
            }

            context
                .buildConstraintViolationWithTemplate("{non-student.group-number.must-be-null}")
                .addPropertyNode("groupNumber")
                .addConstraintViolation();
            return false;
        }
    }
}
