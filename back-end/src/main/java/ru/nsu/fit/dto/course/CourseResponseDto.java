package ru.nsu.fit.dto.course;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

@Data
@Accessors(chain = true)
@ApiModel("Учебный курс")
public class CourseResponseDto {
    @ApiModelProperty(value = "Идентификатор курса", example = "1")
    private int id;

    @ApiModelProperty(value = "Название курса", example = "Математический анализ")
    private String name;

    @ApiModelProperty(value = "Описание курса", example = "Курс об основах математического анализа")
    private String description;

    @ApiModelProperty(value = "Ступень курса", example = "Бакалавриат")
    private CourseLevel level;

    @ApiModelProperty(value = "Статус курса", example = "ACTIVE")
    private CourseStatus status;
}
