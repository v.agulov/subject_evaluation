package ru.nsu.fit.dto.survey;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

import static ru.nsu.fit.util.FormPatterns.DATE_FORMAT;

@Data
@Accessors(chain = true)
@ApiModel("Опрос по курсу")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SurveyResponseDto {

    @ApiModelProperty(value = "ID опроса", example = "1")
    private int id;

    @ApiModelProperty(value = "ID курса, по которому будет проводиться опрос", example = "1")
    private int courseId;

    @ApiModelProperty(value = "ID пользователя, который создал опрос", example = "1")
    private int authorId;

    @ApiModelProperty(value = "Дата начала опроса в формате " + DATE_FORMAT, example = "2020-01-13")
    private String startDate;

    @ApiModelProperty(value = "Дата начала опроса в формате " + DATE_FORMAT, example = "2020-01-13")
    private String endDate;

    @ApiModelProperty(value = "Учебный год в формате yyyy - yyyy", example = "2020 - 2021")
    private String year;

    @ApiModelProperty(value = "Семестр (1 или 2)", example = "1")
    private int semester;

    @ApiModelProperty(value = "ID студентов, для которых создается опрос", example = "[1, 2 ,3]")
    private List<Integer> studentIds;

    @ApiModelProperty(value = "ID учителей, для которых создается опрос", example = "[1, 2 ,3]")
    private List<Integer> teacherIds;

    @ApiModelProperty(value = "Список вопросов, из которых состоит опрос")
    private List<QuestionResponseDto> questions;
}
