package ru.nsu.fit.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@ApiModel(description = "Код авторизации google sign-in")
public class AuthCodeDto {
    @NotBlank
    @ApiModelProperty(value = "Код авторизации google sign-in", required = true)
    private String authCode;
}
