package ru.nsu.fit.dto.feedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.util.feedback.Answer;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FeedbackDto {

    private int id;

    private Instant creationDate;

    private Instant publishingDate;

    private String courseName;

    private String year;

    private Integer semester;

    private FeedbackStatus feedbackStatus;

    private UserDto author;

    private List<UserDto> teachers;

    private List<Answer> answers;

    private Instant startSurveyDate;

    private Instant endSurveyDate;

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class UserDto {

        private String firstName;

        private String lastName;

        private String patronymic;

        private String email;
    }
}
