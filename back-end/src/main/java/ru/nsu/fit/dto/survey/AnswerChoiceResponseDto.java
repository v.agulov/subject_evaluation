package ru.nsu.fit.dto.survey;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("Вариант ответа на вопрос")
public class AnswerChoiceResponseDto {

    @ApiModelProperty(value = "ID варианта ответа", example = "1")
    private int id;

    @ApiModelProperty(value = "Текст варианта ответа на вопрос", example = "Организация курса была отличная")
    private String title;
}
