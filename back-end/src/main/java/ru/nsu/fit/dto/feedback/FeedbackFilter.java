package ru.nsu.fit.dto.feedback;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.FeedbackStatus;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class FeedbackFilter {
    private Set<Long> courseIds;
    private Set<FeedbackStatus> statuses;
}
