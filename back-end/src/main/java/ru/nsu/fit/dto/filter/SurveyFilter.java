package ru.nsu.fit.dto.filter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel("Фильтр для поиска опросов")
public class SurveyFilter {
    @ApiModelProperty(value = "Идентификатор курса", example = "1")
    private Integer courseId;

    @ApiModelProperty(value = "Идентификатор студента", example = "1")
    private Integer studentId;
}
