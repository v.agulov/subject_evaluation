package ru.nsu.fit.configuration;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.sql.DataSource;

@Configuration
@ParametersAreNonnullByDefault
public class LiquibaseConfiguration {
    @Bean
    public SpringLiquibase liquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:migrations/changelog.xml");
        return liquibase;
    }
}
