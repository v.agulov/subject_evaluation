package ru.nsu.fit.validator.survey;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CorrectStudyYearsValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface CorrectStudyYears {

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String message() default "Incorrect study years";

    String year();

}