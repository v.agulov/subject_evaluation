package ru.nsu.fit.service.course;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.enums.CourseStatus;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;

/**
 * Сервис для работы с курсами
 */
@ParametersAreNonnullByDefault
public interface CourseService {
    /**
     * Получение курса по его идентификатору
     *
     * @param id идентификатор курса
     * @return курс
     * @throws DataNotFoundException если курса с таким идентификатором не найдено
     */
    @Nonnull
    Course getCourse(int id) throws DataNotFoundException;

    /**
     * Поиск курсов по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры для поиска
     * @param pageable настройки для пагинации
     * @return страница курсов
     */
    @Nonnull
    Page<Course> searchCourses(CourseFilter filter, Pageable pageable);

    /**
     * Обновление информации о курсе
     *
     * @param id     идентификатор сущности
     * @param course сущность, которую нужно обновить
     * @return обновленная информация о курсе
     */
    @Nonnull
    Course updateCourse(int id, Course course);

    /**
     * Создание курса
     *
     * @param course данные курса
     * @return созданный курс
     */
    @Nonnull
    Course createCourse(Course course);

    /**
     * Изменение статуса одного или нескольких курсов
     *
     * @param idCourses    идентификаторы курсов
     * @param courseStatus статус, на который нужно изменить статус курсов
     */
    void updateCourseStatus(Set<Integer> idCourses, CourseStatus courseStatus);

    /**
     * Получение всех идентификаторов курсов
     *
     * @return список идентификаторов
     */
    List<Integer> getAllCourseIds();

    /**
     * Получение общего статуса для courseIds
     *
     * @param courseIds идентификаторы курсов
     * @return общий статус, либо null
     */
    CourseStatus defineStatusByIds(List<Integer> courseIds);
}
