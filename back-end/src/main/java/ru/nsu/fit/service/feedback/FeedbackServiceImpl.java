package ru.nsu.fit.service.feedback;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dao.FeedbackRepository;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.Feedback;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.exceptions.DataNotFoundException;
import ru.nsu.fit.exceptions.SEAccessDeniedException;
import ru.nsu.fit.service.auth.AuthManager;
import ru.nsu.fit.specification.FeedbackSpecificationFactory;
import ru.nsu.fit.util.CollectionUtils;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Stream;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class FeedbackServiceImpl implements FeedbackService {
    private final static String FEEDBACK_NOT_FOUND = "Не найден отзыв с идентификатором %d";
    private final static String TEACHER_ACCESS_DENIED = "Доступ запрещен. Вы можете модерировать отзывы " +
        "только тех опросов, которые создали сами или в которых являетесь преподавателем";

    private final AuthManager authManager;
    private final FeedbackRepository feedbackRepository;
    private final FeedbackSpecificationFactory feedbackSpecificationFactory;

    @Override
    public void updateStatus(int feedbackId, FeedbackStatus status) throws DataNotFoundException {
        Feedback feedback = feedbackRepository.findById(feedbackId)
            .orElseThrow(() -> new DataNotFoundException(String.format(FEEDBACK_NOT_FOUND, feedbackId)));
        if (authManager.hasRole(Role.TEACHER)) {
            Survey survey = feedback.getSurvey();
            User teacher = authManager.getUser();
            if (!survey.getAuthor().getId().equals(teacher.getId()) &&
                survey.getTeachers().stream().noneMatch(t -> t.getId().equals(teacher.getId()))) {
                throw new SEAccessDeniedException(TEACHER_ACCESS_DENIED);
            }
        }
        feedback.setStatus(status);
    }

    @Override
    public Page<Feedback> searchFeedbacks(FeedbackFilter filter, Pageable pageable) {
        if (
            Stream.<Function<FeedbackFilter, Collection<?>>>of(
                FeedbackFilter::getCourseIds,
                FeedbackFilter::getStatuses
            )
                .map(getter -> getter.apply(filter))
                .anyMatch(CollectionUtils::isNonnullAndEmpty)
        ) {
            return Page.empty();
        }
        return feedbackRepository.findAll(
            feedbackSpecificationFactory.fromFilter(filter),
            pageable
        );
    }

}
