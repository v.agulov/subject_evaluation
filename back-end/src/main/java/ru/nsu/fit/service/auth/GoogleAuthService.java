package ru.nsu.fit.service.auth;

import ru.nsu.fit.dto.user.GoogleUser;
import ru.nsu.fit.entity.User;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Авторизация через Google.
 */
@ParametersAreNonnullByDefault
public interface GoogleAuthService {

    /**
     * Получает информацию о пользователе по коду авторизации Google.
     *
     * @param authCode - код авторизации Google, по нему можно узать access token, refresh token и id_token.
     */
    GoogleUser authorize(String authCode);

    /**
     * Обновить access-токен пользователя с помощью Google api.
     */
    GoogleUser refreshAccessToken(User user);
}
