package ru.nsu.fit.service.survey;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dao.SurveyRepository;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyServiceImpl implements SurveyService {
    private static final String SURVEY_NOT_FOUND = "Не найден опрос с идентификатором ";

    private final SurveyRepository surveyRepository;

    @Nonnull
    @Override
    public Survey getSurvey(int id) {
        return surveyRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException(SURVEY_NOT_FOUND + id));
    }

    @Nonnull
    @Override
    public Survey createSurvey(Survey survey) {
        return surveyRepository.save(survey);
    }

    @Override
    public List<Survey> getSurveysForStartNotificationSending() {
        return surveyRepository.findByStartDateLessThanEqualAndIsStartNotificatedFalse(LocalDate.now());
    }

    @Override
    public List<Survey> getSurveysForEndNotificationSending() {
        return surveyRepository.findByEndDateBeforeAndIsEndNotificatedFalse(LocalDate.now());
    }

    @Override
    public void setSurveyStartNotificated(int surveyId) {
        surveyRepository.findById(surveyId)
            .ifPresent(this::setSurveyStartNotificated);
    }

    @Override
    public void setSurveyEndNotificated(int surveyId) {
        surveyRepository.findById(surveyId)
            .ifPresent(this::setSurveyEndNotificated);
    }

    private void setSurveyStartNotificated(Survey survey) {
        survey.setStartNotificated(true);
        surveyRepository.save(survey);
    }

    private void setSurveyEndNotificated(Survey survey) {
        survey.setEndNotificated(true);
        surveyRepository.save(survey);
    }

    @Nonnull
    @Override
    public Page<Survey> searchSurvey(SurveyFilter filter, Pageable pageable) {
        Course course = null;
        User user = null;

        if (Objects.nonNull(filter.getCourseId())) {
            course = new Course();
            course.setId(filter.getCourseId());
        }

        if (Objects.nonNull(filter.getStudentId())) {
            user = new User(filter.getStudentId());
        }

        return surveyRepository.findByCourseOrStudentsEquals(course, user, pageable);
    }
}
