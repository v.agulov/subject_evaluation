package ru.nsu.fit.service.survey.notification;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.events.publishers.SurveyNotificationPublisher;
import ru.nsu.fit.service.survey.SurveyServiceImpl;

import java.util.List;

import static ru.nsu.fit.events.model.NotificationType.END_SURVEY;
import static ru.nsu.fit.events.model.NotificationType.START_SURVEY;

@Log4j2
@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final SurveyServiceImpl surveyService;
    private final SurveyNotificationPublisher surveyNotificationPublisher;

    @Override
    @Scheduled(cron = "${notify.survey-start.timeout}")
    @SchedulerLock(name = "NotificationServiceImpl_notifyStartSurvey")
    public void notifyStartSurvey() {
        log.info("notifyStartSurvey() started");
        List<Survey> surveys = surveyService.getSurveysForStartNotificationSending();
        for (Survey survey : surveys) {
            surveyNotificationPublisher.publishSurveyNotificationEvent(survey, START_SURVEY);
        }
    }

    @Override
    @Scheduled(cron = "${notify.survey-end.timeout}")
    @SchedulerLock(name = "NotificationServiceImpl_notifyEndSurvey")
    public void notifyEndSurvey() {
        log.info("notifyEndSurvey() started");
        List<Survey> surveys = surveyService.getSurveysForEndNotificationSending();
        for (Survey survey : surveys) {
            surveyNotificationPublisher.publishSurveyNotificationEvent(survey, END_SURVEY);
        }
    }
}
