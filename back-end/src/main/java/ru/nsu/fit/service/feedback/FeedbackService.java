package ru.nsu.fit.service.feedback;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.Feedback;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Сервис для работы с отзывами
 */
@ParametersAreNonnullByDefault
public interface FeedbackService {

    /**
     * Обновление статуса отзыва
     *
     * @param feedbackId идентификатор отзыва
     * @param status     новый статус
     * @throws DataNotFoundException если опрос с данным идентификатором не найден
     */
    void updateStatus(int feedbackId, FeedbackStatus status);

    /**
     * Поиск отзывов.
     *
     * @param filter   параметры фильтрации
     * @param pageable параметры для пагинации
     * @return страница отзывов
     */
    Page<Feedback> searchFeedbacks(FeedbackFilter filter, Pageable pageable);

}
