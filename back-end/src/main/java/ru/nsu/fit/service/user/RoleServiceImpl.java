package ru.nsu.fit.service.user;

import java.util.Map;
import java.util.function.Function;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dao.RolesRepository;
import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class RoleServiceImpl implements RoleService {
    private final RolesRepository rolesRepository;

    @Nonnull
    @Override
    public UserRole getRoleByName(Role name) {
        return rolesRepository.findByName(name);
    }

    @Override
    public Map<Role, UserRole> findAll() {
        return StreamEx.of(rolesRepository.findAll())
            .mapToEntry(UserRole::getName, Function.identity())
            .toMap();
    }
}
