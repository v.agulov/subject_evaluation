package ru.nsu.fit.exceptions;

public class SEAccessDeniedException extends RuntimeException {
    public SEAccessDeniedException(final String message) {
        super(message);
    }

    public SEAccessDeniedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
