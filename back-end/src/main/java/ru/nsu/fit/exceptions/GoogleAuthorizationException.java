package ru.nsu.fit.exceptions;

public class GoogleAuthorizationException extends RuntimeException {
    public GoogleAuthorizationException(Throwable cause) {
        super(cause);
    }

    public GoogleAuthorizationException(String message) {
        super(message);
    }

    public GoogleAuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }
}
