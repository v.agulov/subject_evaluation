package ru.nsu.fit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.Feedback;

import javax.annotation.ParametersAreNonnullByDefault;

@Repository
@ParametersAreNonnullByDefault
public interface FeedbackRepository extends JpaRepository<Feedback, Integer>, JpaSpecificationExecutor<Feedback> {
}
