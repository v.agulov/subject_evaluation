package ru.nsu.fit.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.User;

@Repository
@ParametersAreNonnullByDefault
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmailIgnoreCase(String email);

    Optional<User> findByEmail(String email);

    Optional<User> findByAccessToken(String accessToken);

    Optional<User> findByRefreshToken(String refreshToken);

    Optional<User> findById(Integer id);

    List<User> getByIdIn(Set<Integer> id);
}
