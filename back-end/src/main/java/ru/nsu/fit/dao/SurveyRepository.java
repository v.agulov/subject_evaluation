package ru.nsu.fit.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Integer> {
    List<Survey> findByStartDateLessThanEqualAndIsStartNotificatedFalse(LocalDate date);

    Page<Survey> findByCourseOrStudentsEquals(Course course, User student, Pageable pageable);

    List<Survey> findByEndDateBeforeAndIsEndNotificatedFalse(LocalDate date);
}
