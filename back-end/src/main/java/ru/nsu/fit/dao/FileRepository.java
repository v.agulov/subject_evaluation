package ru.nsu.fit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.entity.File;

public interface FileRepository extends JpaRepository<File, Integer> {
}
