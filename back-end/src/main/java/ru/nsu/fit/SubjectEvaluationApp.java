package ru.nsu.fit;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@EnableSchedulerLock(defaultLockAtMostFor = "${default.lock.at.most.scheduler}")
public class SubjectEvaluationApp {
    public static void main(String[] args) {
        SpringApplication.run(SubjectEvaluationApp.class, args);
    }
}
