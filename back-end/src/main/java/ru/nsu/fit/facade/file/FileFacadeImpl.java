package ru.nsu.fit.facade.file;

import java.io.IOException;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.nsu.fit.dto.file.FileCreateDto;
import ru.nsu.fit.dto.file.FileResponseDto;
import ru.nsu.fit.entity.File;
import ru.nsu.fit.entity.enums.FileType;
import ru.nsu.fit.mapper.FileMapper;
import ru.nsu.fit.service.file.FileService;

@Service
@RequiredArgsConstructor
public class FileFacadeImpl implements FileFacade {
    private final FileService fileService;
    private final FileMapper fileMapper;

    @Override
    public FileResponseDto getFile(int id) {
        return fileMapper.fileToResponseDto(fileService.getFileById(id));
    }

    @Override
    public int saveFile(FileCreateDto fileCreateDto) {
        File file = fileMapper.dtoCreateToFile(fileCreateDto);
        fileService.saveFile(file);
        return file.getId();
    }

    @Override
    public FileResponseDto uploadFile(MultipartFile multipartFile, FileType fileType) throws IOException {
        File file = new File()
            .setName(multipartFile.getOriginalFilename())
            .setMimeType(multipartFile.getContentType())
            .setType(fileType)
            .setContent(multipartFile.getBytes());

        fileService.saveFile(file);
        return fileMapper.fileToResponseDto(file);
    }
}
