package ru.nsu.fit.facade.user;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.enums.UserStatus;
import ru.nsu.fit.exceptions.ConflictException;
import ru.nsu.fit.mapper.UserMapper;
import ru.nsu.fit.service.user.RoleService;
import ru.nsu.fit.service.user.UserService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.entity.enums.UserStatus;
import ru.nsu.fit.service.user.UserService;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Set;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class UserFacadeImpl implements UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;
    private final RoleService roleService;

    @Override
    public void updateUsersStatus(Set<Integer> idUsers, UserStatus userStatus) {
        userService.updateStatusUser(idUsers, userStatus);
    }

    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        Optional<User> optionalUser = userService.findByEmail(userRequestDto.getEmail());
        if (optionalUser.isPresent()) {
            throw new ConflictException(String.format(
                "User with email = '%s' already exists",
                userRequestDto.getEmail()
            ));
        }

        User user = userMapper.mapToEntity(new User(), userRequestDto, roleService.findAll());
        User savedUser = userService.save(user);
        return userMapper.mapToApi(savedUser);
    }

    @Override
    public UserResponseDto updateUser(Integer id, UserRequestDto userRequestDto) {
        Optional<User> optionalUser = userService.findByEmail(userRequestDto.getEmail());
        if (optionalUser.isPresent() && !Objects.equals(optionalUser.get().getId(), id)) {
            throw new ConflictException(String.format(
                "User with email = '%s' already exists",
                userRequestDto.getEmail()
            ));
        }

        return userMapper.mapToApi(userService.save(
            userMapper.mapToEntity(userService.getById(id), userRequestDto, roleService.findAll())
        ));
    }
}
