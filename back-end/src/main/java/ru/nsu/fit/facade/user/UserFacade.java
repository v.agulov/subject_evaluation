package ru.nsu.fit.facade.user;

import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;

import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.enums.UserStatus;

/**
 * Сервис для работы с пользователями
 */
@ParametersAreNonnullByDefault
public interface UserFacade {
    /**
     * Изменение статуса пользователя
     *
     * @param idUsers    - идентификаторы пользователей
     * @param userStatus - статус пользователя, на который нужно обновить текущий статус
     */
    void updateUsersStatus(Set<Integer> idUsers, UserStatus userStatus);

    /**
     * Создать пользователя.
     *
     * @param userRequestDto информация о новом пользователе
     * @return информация о созданном пользователе
     */
    UserResponseDto createUser(UserRequestDto userRequestDto);

    /**
     * Обновить пользователя
     *
     * @param userId         идентификатор пользователя
     * @param userRequestDto информация для обновления пользователя
     * @return информация об обновленном пользователе
     */
    UserResponseDto updateUser(Integer userId, UserRequestDto userRequestDto);
}
