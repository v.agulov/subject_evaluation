package ru.nsu.fit.facade.survey;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.mapper.survey.SurveyMapper;
import ru.nsu.fit.service.course.CourseService;
import ru.nsu.fit.service.survey.SurveyService;
import ru.nsu.fit.service.user.UserService;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyFacadeImpl implements SurveyFacade {

    private final UserService userService;
    private final CourseService courseService;
    private final SurveyService surveyService;
    private final SurveyMapper surveyMapper;

    @Nonnull
    @Override
    public SurveyResponseDto getSurvey(int id) {
        Survey survey = surveyService.getSurvey(id);
        return surveyMapper.survey2Dto(survey);
    }

    @Nonnull
    @Override
    public SurveyResponseDto createSurvey(SurveyRequestDto surveyRequestDto) {
        Survey creatingSurvey = surveyMapper.dtoToSurvey(surveyRequestDto);
        User author = userService.getById(surveyRequestDto.getAuthorId());
        Course course = courseService.getCourse(surveyRequestDto.getCourseId());
        Set<User> students = surveyRequestDto.getStudentIds().stream()
            .map(userService::getById)
            .collect(Collectors.toSet());
        Set<User> teachers = surveyRequestDto.getTeacherIds().stream()
            .map(userService::getById)
            .collect(Collectors.toSet());
        creatingSurvey
            .setAuthor(author)
            .setCourse(course)
            .setStudents(students)
            .setTeachers(teachers);
        Survey savedSurvey = surveyService.createSurvey(creatingSurvey);
        SurveyResponseDto responseDto = surveyMapper.survey2ResponseDto(savedSurvey);
        return responseDto
            .setAuthorId(surveyRequestDto.getAuthorId())
            .setCourseId(surveyRequestDto.getCourseId())
            .setStudentIds(surveyRequestDto.getStudentIds())
            .setTeacherIds(surveyRequestDto.getTeacherIds());
    }

    @Nonnull
    @Override
    public Page<SurveyResponseDto> searchSurvey(SurveyFilter filter, Pageable pageable) {
        Page<Survey> surveys = surveyService.searchSurvey(filter, pageable);
        return surveyMapper.surveys2Dtos(surveys);
    }
}
