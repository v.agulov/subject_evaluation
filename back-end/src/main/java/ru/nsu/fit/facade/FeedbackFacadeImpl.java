package ru.nsu.fit.facade;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.mapper.feedback.FeedbackMapper;
import ru.nsu.fit.service.feedback.FeedbackService;

import javax.annotation.ParametersAreNonnullByDefault;

@Service
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class FeedbackFacadeImpl implements FeedbackFacade {

    private final FeedbackService feedbackService;
    private final FeedbackMapper feedbackMapper;

    @Override
    public void updateStatus(int feedbackId, FeedbackStatus status) {
        feedbackService.updateStatus(feedbackId, status);
    }

    @Override
    @Transactional
    public Page<FeedbackDto> searchFeedbacks(FeedbackFilter filter, Pageable pageable) {
        return feedbackService.searchFeedbacks(filter, pageable).map(feedbackMapper::map);
    }

}
