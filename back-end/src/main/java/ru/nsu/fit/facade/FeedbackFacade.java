package ru.nsu.fit.facade;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.enums.FeedbackStatus;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;

/**
 * Сервис для работы с отзывами
 */
@ParametersAreNonnullByDefault
@Transactional
public interface FeedbackFacade {

    /**
     * Обновление статуса отзыва
     *
     * @param feedbackId идентификатор отзыва
     * @param status     новый статус
     */
    void updateStatus(int feedbackId, FeedbackStatus status);

    /**
     * Поиск отзывов.
     *
     * @param filter   параметры фильтрации
     * @param pageable параметры для пагинации
     * @return страница отзывов
     */
    Page<FeedbackDto> searchFeedbacks(FeedbackFilter filter, Pageable pageable);

}
