package ru.nsu.fit.facade.survey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public interface SurveyFacade {
    /**
     * Получение опроса по его идентификатору
     *
     * @param id идентификатор опроса
     * @return информация об опросе
     */
    @Nonnull
    SurveyResponseDto getSurvey(int id);

    /**
     * Создание опроса
     *
     * @param surveyRequestDto данные опроса
     * @return сохраенный опрос со всеми id
     */
    @Nonnull
    SurveyResponseDto createSurvey(SurveyRequestDto surveyRequestDto);

    /**
     * Поиск опросов по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры поиска
     * @param pageable настройки постраничной навигации
     * @return страница опросов
     */
    @Nonnull
    Page<SurveyResponseDto> searchSurvey(SurveyFilter filter, Pageable pageable);
}
