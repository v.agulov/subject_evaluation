package ru.nsu.fit.facade.course;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.course.CourseRequestDto;
import ru.nsu.fit.dto.course.CourseResponseDto;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.enums.CourseStatus;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;


/**
 * Фасад для работы с курсами
 */
@ParametersAreNonnullByDefault
public interface CourseFacade {
    /**
     * Получение курса по его идентификатору
     *
     * @param id идентификатор курса
     * @return курс
     */
    @Nonnull
    CourseResponseDto getCourse(int id);

    /**
     * Создание курса
     *
     * @param courseResponseDto данные курса
     * @return сохраненный курс
     */
    CourseResponseDto createCourse(CourseRequestDto courseResponseDto);

    /**
     * Обновление информации о курсе
     *
     * @param id                     идентификатор курса
     * @param updateCourseRequestDto обновляемые данные
     * @return обновленные данные по курсу
     */
    CourseResponseDto updateCourse(int id, CourseRequestDto updateCourseRequestDto);

    /**
     * Поиск курсов по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры для поиска
     * @param pageable настройки для пагинации
     * @return страница курсов
     */
    @Nonnull
    Page<CourseResponseDto> searchCourses(CourseFilter filter, Pageable pageable);

    /**
     * Изменение статуса одного или нескольких курсов
     *
     * @param idCourses    идентификаторы курсов
     * @param courseStatus статус, на который нужно изменить статусы курсов
     */
    void updateCoursesStatus(Set<Integer> idCourses, CourseStatus courseStatus);

    /**
     * Получение всех идентификаторов курсов
     *
     * @return список идентификаторов
     */
    List<Integer> getAllCourseIds();

    /**
     * Получение общего статуса для courseIds
     *
     * @param courseIds идентификаторы курсов
     * @return общий статус, либо null
     */
    CourseStatus defineStatusByIds(List<Integer> courseIds);
}
