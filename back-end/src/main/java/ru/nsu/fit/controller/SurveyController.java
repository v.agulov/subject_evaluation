package ru.nsu.fit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.facade.survey.SurveyFacade;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static ru.nsu.fit.util.paths.SurveyPaths.*;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@RequestMapping(SURVEY_ROOT_PATH)
@Api("API для работы с опросами")
public class SurveyController {

    private final SurveyFacade surveyFacade;

    @GetMapping(SURVEY_GET_PATH)
    @ApiOperation(value = "Получение опроса по его идентификатору")
    public SurveyResponseDto getSurvey(
        @ApiParam(required = true, value = "Идентификатор опроса") @PathVariable int id) {
        return surveyFacade.getSurvey(id);
    }

    @PostMapping
    @ApiOperation(value = "Создание опроса")
    public SurveyResponseDto createSurvey(@RequestBody @Valid @NotNull SurveyRequestDto surveyRequestDto) {
        log.debug("Create survey method queried with dto {}", surveyRequestDto);
        return surveyFacade.createSurvey(surveyRequestDto);
    }

    @PostMapping(SURVEY_SEARCH_PATH)
    @ApiOperation(value = "Получение списка опросов")
    public Page<SurveyResponseDto> searchSurveys(
        @ApiParam(required = true, value = "Фильтр для поиска опросов") @RequestBody SurveyFilter filter,
        @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.ASC)
        @ApiParam(value = "Параметры для постраничной навигации") Pageable pageable
    ) {
        return surveyFacade.searchSurvey(filter, pageable);
    }

}
