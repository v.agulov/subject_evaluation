package ru.nsu.fit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.dto.feedback.FeedbackStatusUpdateDto;
import ru.nsu.fit.facade.FeedbackFacade;
import ru.nsu.fit.util.paths.FeedbackPaths;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;

@Api("API для работы с отзывами")
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(FeedbackPaths.FEEDBACK_ROOT_PATH)
@ParametersAreNonnullByDefault
public class FeedbackController {

    private final FeedbackFacade feedbackFacade;

    @PostMapping(FeedbackPaths.FEEDBACK_UPDATE_STATUS_PATH)
    @ApiOperation(value = "Обновление статуса отзыва")
    public void updateStatus(
        @ApiParam(required = true, value = "Идентификатор отзыва") @PathVariable int feedbackId,
        @ApiParam(required = true, value = "Новый статус") @RequestBody FeedbackStatusUpdateDto body
    ) {
        feedbackFacade.updateStatus(feedbackId, body.getStatus());
    }

    @PutMapping(FeedbackPaths.FEEDBACK_SEARCH_PATH)
    public Page<FeedbackDto> searchFeedbacks(
        @Valid @RequestBody FeedbackFilter feedbackFilter,
        @PageableDefault(sort = {"creationDate"}, direction = Sort.Direction.DESC)
        @ApiParam(value = "Параметры для пагинации") Pageable pageable
    ) {
        return feedbackFacade.searchFeedbacks(feedbackFilter, pageable);
    }
}
