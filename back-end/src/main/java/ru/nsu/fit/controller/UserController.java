package ru.nsu.fit.controller;

import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.enums.UserStatus;
import ru.nsu.fit.facade.user.UserFacade;

import static ru.nsu.fit.util.paths.UserPaths.*;

@Api("API для работы с пользователями")
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(USERS_ROOT_PATH)
@ParametersAreNonnullByDefault
public class UserController {
    private final UserFacade userFacade;

    @PostMapping(USERS_BLOCK_PATH)
    @ApiOperation(value = "Блокировка пользователей")
    public void blockUsers(@RequestBody Set<Integer> idUsers) {
        userFacade.updateUsersStatus(idUsers, UserStatus.BLOCKED);
    }

    @PostMapping(USERS_ACTIVATE_PATH)
    @ApiOperation(value = "Активирование пользователей")
    public void activateUsers(@RequestBody Set<Integer> idUsers) {
        userFacade.updateUsersStatus(idUsers, UserStatus.ACTIVE);
    }

    @PostMapping
    public UserResponseDto createUser(@RequestBody @Valid UserRequestDto userRequestDto) {
        return userFacade.createUser(userRequestDto);
    }

    @PutMapping(USERS_UPDATE_PATH)
    public UserResponseDto updateUser(
        @PathVariable Integer userId,
        @RequestBody @Valid UserRequestDto userRequestDto
    ) {
        return userFacade.updateUser(userId, userRequestDto);
    }
}
