package ru.nsu.fit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.dto.course.CourseRequestDto;
import ru.nsu.fit.dto.course.CourseResponseDto;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.enums.CourseStatus;
import ru.nsu.fit.facade.course.CourseFacade;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

import static ru.nsu.fit.util.paths.CoursePaths.*;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@Api("API для работы с курсами")
@RequestMapping(COURSES_ROOT_PATH)
public class CourseController {
    private final CourseFacade courseFacade;

    @GetMapping(COURSES_GET_PATH)
    @ApiOperation(value = "Получение курса по его идентификатору")
    public CourseResponseDto getCourse(@ApiParam(required = true, value = "Идентификатор курса") @PathVariable int id) {
        return courseFacade.getCourse(id);
    }

    @PutMapping(COURSES_SEARCH_PATH)
    @ApiOperation(value = "Получение списка курсов")
    public Page<CourseResponseDto> searchCourses(
        @ApiParam(required = true, value = "Фильтр для поиска курсов") @RequestBody CourseFilter filter,
        @PageableDefault(sort = {"status", "name"}, direction = Sort.Direction.ASC)
        @ApiParam(value = "Параметры для пагинации") Pageable pageable
    ) {
        return courseFacade.searchCourses(filter, pageable);
    }

    @PostMapping
    @ApiOperation(value = "Создание курса")
    public CourseResponseDto createCourse(@RequestBody @Valid @NotNull CourseRequestDto course) {
        return courseFacade.createCourse(course);
    }

    @PutMapping(COURSES_UPDATE_PATH)
    @ApiOperation(value = "Изменение курса")
    public CourseResponseDto updateCourse(
        @ApiParam(required = true, value = "Идентификатор курса") @PathVariable int id,
        @ApiParam(required = true, value = "Курс") @RequestBody @Valid @NotNull CourseRequestDto course
    ) {
        return courseFacade.updateCourse(id, course);
    }

    @PostMapping(COURSES_ARCHIVE_PATH)
    @ApiOperation(value = "Архивация курсов")
    public void archiveCourses(@RequestBody Set<Integer> idCourses) {
        courseFacade.updateCoursesStatus(idCourses, CourseStatus.ARCHIVED);
    }

    @PostMapping(COURSES_UNARCHIVE_PATH)
    @ApiOperation(value = "Разархивация курсов")
    public void unarchiveCourses(@RequestBody Set<Integer> idCourses) {
        courseFacade.updateCoursesStatus(idCourses, CourseStatus.ACTIVE);
    }

    @GetMapping(COURSES_ALL_IDS_PATH)
    @ApiOperation(value = "Получение всех идентификаторов курсов")
    public List<Integer> getAllCourseIds() {
        return courseFacade.getAllCourseIds();
    }

    @GetMapping(COURSES_IDS_STATUS_PATH)
    @ApiOperation(value = "Получение общего статуса по идентификаторам курсов")
    public CourseStatus defineCourseStatus(@PathVariable List<Integer> courseIds) {
        return courseFacade.defineStatusByIds(courseIds);
    }
}
