package ru.nsu.fit.specification;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.jpa.domain.Specification;

@ParametersAreNonnullByDefault
public interface SpecificationFactory<T, U> {
    @Nonnull
    Specification<U> fromFilter(T filter);
}
