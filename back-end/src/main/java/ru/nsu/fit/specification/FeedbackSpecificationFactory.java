package ru.nsu.fit.specification;

import javax.annotation.ParametersAreNonnullByDefault;

import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.Feedback;

@ParametersAreNonnullByDefault
public interface FeedbackSpecificationFactory extends SpecificationFactory<FeedbackFilter, Feedback> {
}
