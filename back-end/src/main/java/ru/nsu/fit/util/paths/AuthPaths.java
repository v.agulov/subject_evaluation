package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_PREFIX;

@UtilityClass
public class AuthPaths {
    public final static String AUTH_PATH = "/auth";
    public final static String LOGOUT_PATH = "/logout";
    public final static String REFRESH_PATH = "/refresh";
    public final static String ROOT_AUTH_PATH = API_VERSION_PREFIX + AUTH_PATH;
    public final static String ROOT_LOGOUT_PATH = API_VERSION_PREFIX + LOGOUT_PATH;
    public final static String ROOT_REFRESH_PATH = API_VERSION_PREFIX + REFRESH_PATH;
}
