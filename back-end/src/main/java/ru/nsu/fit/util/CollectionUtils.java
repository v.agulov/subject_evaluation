package ru.nsu.fit.util;

import java.util.Collection;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CollectionUtils {
    public boolean isNonnullAndEmpty(Collection<?> collection) {
        return collection != null && collection.isEmpty();
    }
}
