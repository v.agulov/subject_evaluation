package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RootPaths {
    public final static String API_VERSION_PREFIX = "/api/v1";
    public final static String API_VERSION_SECURE_PREFIX = "/api/v1/secure";
}
