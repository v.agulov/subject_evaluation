package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_PREFIX;

@UtilityClass
public class FilePaths {
    public static final String FILES_ROOT_PATH = API_VERSION_PREFIX + "/files";
    public static final String FILES_SAVE_PATH = "save";
    public static final String FILES_UPLOAD_PATH = "upload";
    public static final String FILES_GET_PATH = "{id}";
    public static final String FILES_DOWNLOAD_PATH = "{id}/download";
}
