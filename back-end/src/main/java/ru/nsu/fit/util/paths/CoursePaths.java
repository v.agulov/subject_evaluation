package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

@UtilityClass
public class CoursePaths {
    public static final String COURSES_ROOT_PATH = API_VERSION_SECURE_PREFIX + "/courses";
    public static final String COURSES_GET_PATH = "{id}";
    public static final String COURSES_SEARCH_PATH = "search";
    public static final String COURSES_UPDATE_PATH = "{id}";
    public static final String COURSES_ARCHIVE_PATH = "archive";
    public static final String COURSES_UNARCHIVE_PATH = "unarchive";
    public static final String COURSES_ALL_IDS_PATH = "all-ids";
    public static final String COURSES_IDS_STATUS_PATH = "status/{courseIds}";
}
