package ru.nsu.fit.events.publishers;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.nsu.fit.events.model.NotificationType;
import ru.nsu.fit.events.model.SurveyNotificationSuccessEvent;

import javax.annotation.ParametersAreNonnullByDefault;

@Log4j2
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyNotificationSuccessEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishSurveyNotificationSuccess(int surveyId, NotificationType notificationType){
        log.info("Publishing survey notification success event.");
        SurveyNotificationSuccessEvent event = new SurveyNotificationSuccessEvent(surveyId, notificationType);
        applicationEventPublisher.publishEvent(event);
    }
}
