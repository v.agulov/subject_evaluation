package ru.nsu.fit.events.model;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import javax.annotation.ParametersAreNonnullByDefault;

@Getter
@ParametersAreNonnullByDefault
public class SurveyNotificationSuccessEvent extends ApplicationEvent {
   private final NotificationType notificationType;
   private final int surveyId;

    public SurveyNotificationSuccessEvent(Integer surveyId, NotificationType notificationType) {
        super(surveyId);
        this.notificationType = notificationType;
        this.surveyId = surveyId;
    }
}
