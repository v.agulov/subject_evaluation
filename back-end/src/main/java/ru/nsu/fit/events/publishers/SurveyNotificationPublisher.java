package ru.nsu.fit.events.publishers;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.events.model.EndSurveyNotificationEvent;
import ru.nsu.fit.events.model.NotificationType;
import ru.nsu.fit.events.model.StartSurveyNotificationEvent;
import ru.nsu.fit.events.model.SurveyNotificationEvent;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.nsu.fit.events.model.NotificationType.START_SURVEY;


@Log4j2
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyNotificationPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishSurveyNotificationEvent(final Survey survey,
                                               final NotificationType notificationType) {
        List<String> emails = getEmailsForNotification(survey);
        if (emails.isEmpty()) {
            return;
        }
        log.info("Publishing survey notification event. " + notificationType);
        SurveyNotificationEvent notificationEvent;
        String courseName = survey.getCourse().getName();
        if (START_SURVEY.equals(notificationType)) {
            notificationEvent = new StartSurveyNotificationEvent(emails, courseName, survey.getId());
        } else {
            notificationEvent = new EndSurveyNotificationEvent(emails, courseName, survey.getId());
        }
        applicationEventPublisher.publishEvent(notificationEvent);
    }

    @Nonnull
    public List<String> getEmailsForNotification(Survey survey) {
        Set<User> users = survey.getStudents();
        if (users == null) {
            return Collections.emptyList();
        }
        return users.stream().map(User::getEmail).collect(Collectors.toList());
    }
}
