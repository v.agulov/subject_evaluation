package ru.nsu.fit.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import ru.nsu.fit.dto.course.CourseRequestDto;
import ru.nsu.fit.dto.course.CourseResponseDto;
import ru.nsu.fit.entity.Course;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CourseMapper {
    CourseResponseDto courseToDto(Course course);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "files", ignore = true)
    Course dtoToCourse(CourseRequestDto courseResponseDto);

    @Nonnull
    default Page<CourseResponseDto> coursesToDtos(@Nonnull Page<Course> coursesPage) {
        return coursesPage.map(this::courseToDto);
    }
}
