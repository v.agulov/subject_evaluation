package ru.nsu.fit.mapper.survey;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.survey.QuestionRequestDto;
import ru.nsu.fit.dto.survey.QuestionResponseDto;
import ru.nsu.fit.entity.question.MultipleOptionsQuestion;
import ru.nsu.fit.entity.question.OpenQuestion;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.entity.question.SingleOptionQuestion;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class QuestionMapper {

    private final AnswerChoiceMapper answerChoiceMapper;

    @Nonnull
    public Question dtoToQuestion(QuestionRequestDto questionRequestDto) {
        Question question;
        switch (questionRequestDto.getType()) {
            case SINGLE_OPTION:
                SingleOptionQuestion singleOptionQuestion = new SingleOptionQuestion();
                singleOptionQuestion.setAnswerChoices(
                    answerChoiceMapper.dtosToAnswerChoices(questionRequestDto.getAnswerChoices())
                );
                question = singleOptionQuestion;
                break;
            case MULTIPLE_OPTIONS:
                MultipleOptionsQuestion multipleOptionsQuestion = new MultipleOptionsQuestion();
                multipleOptionsQuestion.setAnswerChoices(
                    answerChoiceMapper.dtosToAnswerChoices(questionRequestDto.getAnswerChoices())
                );
                question = multipleOptionsQuestion;
                break;
            case OPEN:
                question = new OpenQuestion();
                break;
            default:
                throw new IllegalStateException("Unknown question type");
        }
        return question
            .setTitle(questionRequestDto.getTitle());
    }

    @Nonnull
    public List<Question> dtosToQuestions(List<QuestionRequestDto> questionRequestDtos) {
        return questionRequestDtos.stream()
            .map(this::dtoToQuestion)
            .collect(Collectors.toList());
    }

    @Nonnull
    public QuestionResponseDto questionToDto(Question question) {
        QuestionResponseDto dto = new QuestionResponseDto()
            .setId(question.getId())
            .setType(question.getQuestionType())
            .setTitle(question.getTitle());

        if (question instanceof SingleOptionQuestion) {
            SingleOptionQuestion singleOptionQuestion = (SingleOptionQuestion) question;
            dto.setAnswerChoices(
                answerChoiceMapper.answerChoicesToDtos(singleOptionQuestion.getAnswerChoices())
            );
        } else if (question instanceof MultipleOptionsQuestion) {
            MultipleOptionsQuestion singleOptionQuestion = (MultipleOptionsQuestion) question;
            dto.setAnswerChoices(
                answerChoiceMapper.answerChoicesToDtos(singleOptionQuestion.getAnswerChoices())
            );
        }
        return dto;
    }

    @Nonnull
    public List<QuestionResponseDto> questionsToDtos(List<Question> questions) {
        return questions.stream()
            .map(this::questionToDto)
            .collect(Collectors.toList());
    }
}
