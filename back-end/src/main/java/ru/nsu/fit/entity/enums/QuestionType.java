package ru.nsu.fit.entity.enums;

public enum QuestionType {
    OPEN,
    SINGLE_OPTION,
    MULTIPLE_OPTIONS
}
