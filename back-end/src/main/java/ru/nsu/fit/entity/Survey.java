package ru.nsu.fit.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.question.Question;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Table(name = "surveys")
public class Survey extends Identifiable {

    private LocalDate startDate;

    private LocalDate endDate;

    private String year;

    private Integer semester;

    private boolean isStartNotificated;

    private boolean isEndNotificated;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToMany
    @JoinTable(
        name = "survey_teachers",
        joinColumns = @JoinColumn(name = "survey_id"),
        inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    private Set<User> teachers;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "survey_students",
        joinColumns = @JoinColumn(name = "survey_id"),
        inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<User> students;

    @OneToMany(mappedBy = "survey")
    private List<Feedback> feedbacks;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.PERSIST)
    private List<Question> questions;

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
        questions.forEach(
            question -> question.setSurvey(this)
        );
    }
}


