package ru.nsu.fit.entity.enums;

public enum CourseLevel {
    UNDERGRADUATE,
    MAGISTRACY,
    POSTGRADUATE,
}
