package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@DiscriminatorValue("SINGLE_OPTION")
public class SingleOptionQuestion extends Question {

    @OneToMany(mappedBy = "question", cascade = CascadeType.PERSIST)
    private List<AnswerChoice> answerChoices;

    @Override
    public QuestionType getQuestionType() {
        return QuestionType.SINGLE_OPTION;
    }

    public void setAnswerChoices(List<AnswerChoice> answerChoices) {
        this.answerChoices = answerChoices;
        answerChoices.forEach(
            answerChoice -> answerChoice.setQuestion(this)
        );
    }

}
