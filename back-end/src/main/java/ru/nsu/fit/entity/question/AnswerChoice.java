package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.Identifiable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Table(name = "answer_choices")
public class AnswerChoice extends Identifiable {

    private String title;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
}
