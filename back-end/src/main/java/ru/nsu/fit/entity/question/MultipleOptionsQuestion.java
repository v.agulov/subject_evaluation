package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@DiscriminatorValue("MULTIPLE_OPTIONS")
public class MultipleOptionsQuestion extends Question {

    @OneToMany(mappedBy = "question", cascade = CascadeType.PERSIST)
    private List<AnswerChoice> answerChoices;

    @Override
    public QuestionType getQuestionType() {
        return QuestionType.MULTIPLE_OPTIONS;
    }

    public void setAnswerChoices(List<AnswerChoice> answerChoices) {
        this.answerChoices = answerChoices;
        answerChoices.forEach(
            answerChoice -> answerChoice.setQuestion(this)
        );
    }

}
