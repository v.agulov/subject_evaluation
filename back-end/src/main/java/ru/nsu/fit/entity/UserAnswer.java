package ru.nsu.fit.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.entity.question.AnswerChoice;
import ru.nsu.fit.entity.question.Question;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "user_answers")
public class UserAnswer extends Identifiable {

    private String answerText;

    @ManyToOne
    @JoinColumn(name = "feedback_id")
    private Feedback feedback;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    @ManyToMany
    @JoinTable(
        name = "user_answer_choices",
        joinColumns = @JoinColumn(name = "user_answer_id"),
        inverseJoinColumns = @JoinColumn(name = "answer_choice_id")
    )
    private List<AnswerChoice> pickedAnswerChoices;

}
