package ru.nsu.fit.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import ru.nsu.fit.entity.enums.Role;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Table(name = "roles")
public class UserRole extends Identifiable implements GrantedAuthority {

    @Enumerated(EnumType.STRING)
    private Role name;

    @Override
    public String getAuthority() {
        return name.getAuthority();
    }

}
