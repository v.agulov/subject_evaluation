package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("OPEN")
public class OpenQuestion extends Question {

    @Override
    public QuestionType getQuestionType() {
        return QuestionType.OPEN;
    }
}
