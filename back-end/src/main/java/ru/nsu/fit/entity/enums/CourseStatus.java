package ru.nsu.fit.entity.enums;

public enum CourseStatus {
    ACTIVE,
    ARCHIVED,
}
