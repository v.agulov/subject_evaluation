package ru.nsu.fit.entity.enums;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
}
