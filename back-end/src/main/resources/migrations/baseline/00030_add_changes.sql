-- noinspection SqlNoDataSourceInspectionForFile

--liquibase formatted sql
--changeset shevchuk_ekaterina: making changes to the database

ALTER TABLE files ADD COLUMN content bytea NOT NULL;
ALTER TABLE files DROP COLUMN course_id;
CREATE TABLE course_files(
    course_id INTEGER NOT NULL REFERENCES courses(id),
    file_id INTEGER NOT NULL REFERENCES files(id))
