-- noinspection SqlNoDataSourceInspectionForFile

--liquibase formatted sql
--changeset smirnov_alexander: add tokens

CREATE TABLE questions (
    id SERIAL PRIMARY KEY,
    survey_id INTEGER NOT NULL REFERENCES surveys(id),
    title TEXT NOT NULL,
    type VARCHAR(50) NOT NULL
);

CREATE TABLE answer_choices (
    id SERIAL PRIMARY KEY,
    question_id INTEGER NOT NULL REFERENCES questions(id),
    title TEXT NOT NULL
);

CREATE TABLE user_answers (
    id SERIAL PRIMARY KEY,
    feedback_id INTEGER NOT NULL REFERENCES feedbacks(id),
    question_id INTEGER NOT NULL REFERENCES questions(id),
    answer_text TEXT -- для вариантов ответа на вопрос "свой ответ", а также для открытых вопросов
);

CREATE TABLE user_answer_choices (
    user_answer_id INTEGER NOT NULL REFERENCES user_answers(id),
    answer_choice_id INTEGER NOT NULL REFERENCES answer_choices(id),
    PRIMARY KEY (user_answer_id, answer_choice_id)
);

ALTER TABLE surveys
    DROP COLUMN year;

ALTER TABLE surveys
    ADD COLUMN year VARCHAR(20);

ALTER TABLE surveys
    ALTER COLUMN start_date TYPE DATE;

ALTER TABLE surveys
    ALTER COLUMN end_date TYPE DATE;