-- noinspection SqlNoDataSourceInspectionForFile

--liquibase formatted sql
--changeset smirnov_alexander: add tokens

ALTER TABLE users
    ADD COLUMN google_id TEXT;

ALTER TABLE users
    ADD COLUMN access_token TEXT;

ALTER TABLE users
    ADD COLUMN refresh_token TEXT;

ALTER TABLE users
    ADD COLUMN access_token_expire_date TIMESTAMP(0) WITH TIME ZONE;

CREATE UNIQUE INDEX users_token_index
    ON users(access_token);