import React from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {useAuth} from "./components/Auth/AuthProvider";
import Authorization from "./components/pages/Authorization";
import Home from "./components/pages/Home";
import CoursesPage from "./components/course/CoursesPage";
import UsersPage from "./components/pages/Users/UsersPage";
import ViewCourse, {about} from "./components/pages/ViewCourse";
import ModerationPage from "./components/feedback/moderation/ModerationPage";

const Routes: React.FC = () => {
  const [logged] = useAuth();

  return (
    <BrowserRouter>
      <Switch>
        {!logged && (
          <>
            <Route exact path="/auth" component={Authorization}/>
            <Redirect to="/auth"/>
          </>
        )}
        {logged && (
          <Home>
            <Route exact path="/courses" component={CoursesPage}/>
            <Route path="/user-page" component={UsersPage}/>
            <Route
              path="/courses/:id"
              render={(props) => <ViewCourse about={about.Description} courseId={props.match.params.id}/>}
            />
            <Route path="/feedback/moderate" component={ModerationPage}/>
            <Redirect to="/courses"/>
          </Home>
        )}
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;