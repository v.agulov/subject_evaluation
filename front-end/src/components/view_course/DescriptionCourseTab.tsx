import React, {useEffect, useState} from "react";
import Button from "@material-ui/core/Button";
import "../styles/ViewCourse.css";
import {TextFieldComponent} from "../course_form/TextFieldComponent";
import {CourseResponseDto} from "../../api/RestApi";
import Api from "../../api/Api";
import {getToken} from "../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../Utils";

interface DescriptionCourseTabProps {
  courseId: number;
}

function DescriptionCourseTab(props: DescriptionCourseTabProps) {
  const [course, setCourse] = useState<CourseResponseDto>();

  useEffect(() => {
    getToken().then(accessToken =>
      Api.getCourse(props.courseId, {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
        .then((response) => {
          setCourse(response.data)
        })
    );
  },);

  let courseDescription: string = '';
  if (course?.description !== undefined)
    courseDescription = course?.description;

  return (
    <div>
      <Button className={"shift-pos"} variant="contained" color="primary">
        Заменить
      </Button>
      <label className={"shift-pos label"}>Описание</label>
      <div className={"text-field"}>
        <TextFieldComponent
          defaultValue={courseDescription}
          rows={200 / 21}
          height={"200px"}
          width={"950px"}
          required={false}
          text={""}
        />
      </div>
    </div>
  );
};

export default DescriptionCourseTab;
