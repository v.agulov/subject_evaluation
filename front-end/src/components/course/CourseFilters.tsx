import React, {Dispatch} from "react";
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel,} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {CourseFilter, CourseLevel, CourseStatus} from "../../api/RestApi";
import {CourseFilterAction, CourseFilterActionType} from "./FilterStore";

const useStyles = makeStyles({
    list: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        "& > * + *": {
            marginTop: "10%",
        },
    },
});

interface FiltersProps {
    filter: CourseFilter;
    dispatch: Dispatch<CourseFilterAction>;
}

const CourseFilters: React.FC<FiltersProps> = ({filter, dispatch}) => {
    const isIndeterminate = filter.levels.length > 0 && filter.levels.length < 3;
    const isAllChecked = filter.levels.length === 3;

    const onChangeLevels = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch({type: CourseFilterActionType.levels, payload: event});
    };

    const onChangeAllLevels = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch({type: CourseFilterActionType.allLevels, payload: event.target.checked});
    };

    const onChangeStatuses = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch({type: CourseFilterActionType.statuses, payload: event});
    };

    const classes = useStyles();
    return (
        <div className={classes.list}>
            <FormControl component="fieldset">
                <FormLabel component="legend">Ступень</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                indeterminate={isIndeterminate}
                                checked={isAllChecked}
                                onChange={onChangeAllLevels}
                            />
                        }
                        label="Все"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={filter.levels.includes(CourseLevel.UNDERGRADUATE)}
                                onChange={onChangeLevels}
                                name={CourseLevel.UNDERGRADUATE}
                            />
                        }
                        label="Бакалавриат"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={filter.levels.includes(CourseLevel.MAGISTRACY)}
                                onChange={onChangeLevels}
                                name={CourseLevel.MAGISTRACY}
                            />
                        }
                        label="Магистратура"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={filter.levels.includes(CourseLevel.POSTGRADUATE)}
                                onChange={onChangeLevels}
                                name={CourseLevel.POSTGRADUATE}
                            />
                        }
                        label="Аспирантура"
                    />
                </FormGroup>
            </FormControl>
            <FormControl component="fieldset">
                <FormLabel component="legend">Статус</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={filter.statuses.includes(CourseStatus.ACTIVE)}
                                onChange={onChangeStatuses}
                                name={CourseStatus.ACTIVE}
                            />
                        }
                        label="Действующие"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={filter.statuses.includes(CourseStatus.ARCHIVED)}
                                onChange={onChangeStatuses}
                                name={CourseStatus.ARCHIVED}
                            />
                        }
                        label="В архиве"
                    />
                </FormGroup>
            </FormControl>
        </div>
    );
};

export default CourseFilters;
