import {CourseLevel, CourseStatus} from "../../api/RestApi";

export const $tl = (level: CourseLevel) => {
    switch (level) {
        case CourseLevel.MAGISTRACY:
            return "Магистратура";
        case CourseLevel.POSTGRADUATE:
            return "Аспирантура";
        case CourseLevel.UNDERGRADUATE:
            return "Бакалавриат";
    }
};

export const $ts = (status: CourseStatus) => {
    switch (status) {
        case CourseStatus.ACTIVE:
            return "Действующий";
        case CourseStatus.ARCHIVED:
            return "В архиве";
    }
};