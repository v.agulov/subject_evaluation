import React from "react";
import {CourseFilter, CourseLevel, CourseStatus,} from "../../api/RestApi";

export const InitialCourseFilter: CourseFilter = {
    name: "",
    statuses: [CourseStatus.ACTIVE, CourseStatus.ARCHIVED],
    levels: [
        CourseLevel.UNDERGRADUATE,
        CourseLevel.MAGISTRACY,
        CourseLevel.POSTGRADUATE,
    ],
};

export enum CourseFilterActionType {
    courseName,
    statuses,
    levels,
    allLevels,
}

export interface CourseFilterAction {
    type: CourseFilterActionType;
    payload: any;
}

export const CourseFilterReducer: React.Reducer<CourseFilter, CourseFilterAction> = (
    state,
    action
) => {
    switch (action.type) {
        case CourseFilterActionType.courseName:
            return {...state, name: action.payload};
        case CourseFilterActionType.levels: {
            const event = action.payload;
            const it = CourseLevel[event.target.name as keyof typeof CourseLevel];
            if (event.target.checked) {
                return {...state, levels: [...state.levels, it]};
            } else {
                return {...state, levels: state.levels.filter((l) => l !== it)};
            }
        }
        case CourseFilterActionType.allLevels:
            if (action.payload) {
                return {...state, levels: InitialCourseFilter.levels};
            } else {
                return {...state, levels: []};
            }
        case CourseFilterActionType.statuses: {
            const event = action.payload;
            const it = CourseStatus[event.target.name as keyof typeof CourseStatus];
            if (event.target.checked) {
                return {...state, statuses: [...state.statuses, it]};
            } else {
                return {...state, statuses: state.statuses.filter((s) => s !== it)};
            }
        }
    }
    return state;
};