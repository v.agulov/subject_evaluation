import {Link} from "react-router-dom";
import {ArrowBackIos} from "@material-ui/icons";
import React from "react";

interface SubjectFormProps {
  headerName: string;
}

export const Header = ({headerName}: SubjectFormProps) => {
  return (
    <div className="header-container">
      <Link to="/courses">
        <ArrowBackIos/>
      </Link>
      <p className="header">{headerName}</p>
    </div>
  );
};
