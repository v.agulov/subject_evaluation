import React from "react";
import { TextFieldComponent } from "./TextFieldComponent";
import "../styles/SubjectForm.css";
import Button from "@material-ui/core/Button";
import FileUploader from "./FileUploader";
import { Header } from "./Header";

interface SubjectFormProps {
  headerName: string;
}

const buttonStyle = {
  marginTop: "20x",
  marginLeft: "10px",
  width: "100px",
  height: "25px",
  fontSize: "8pt",
  left: "210px",
};

export const CourseForm = ({ headerName }: SubjectFormProps) => {
  return (
    <div className="main-container">
      <Header headerName={headerName} />
      <TextFieldComponent
        rows={1}
        defaultValue={""}
        height={"40px"}
        width={"600px"}
        required={true}
        text={"Название"}
      />
      <TextFieldComponent
        rows={300 / 21}
        defaultValue={""}
        height={"300px"}
        width={"600px"}
        required={false}
        text={"Описание"}
      />
      <FileUploader />
      <Button variant="outlined" style={buttonStyle}>
        Отмена
      </Button>
      <Button variant="contained" style={buttonStyle} color="primary">
        Сохранить
      </Button>
    </div>
  );
};
