import React from "react";
import { TextField } from "@material-ui/core";

interface BaseTextFieldProps {
  height: string;
  width: string;
  text: string;
  defaultValue: string;
  required: boolean;
  rows: number;
}

export const TextFieldComponent = ({
  text,
  defaultValue,
  height,
  width,
  required,
  rows,
}: BaseTextFieldProps) => {
  const textFieldStyle = {
    width: width,
    height: height,
    defaultValue: defaultValue,
  };

  const props = {
    style: textFieldStyle,
    multiline: true,
    variant: "outlined",
    rows: rows,
  };

  return (
    <div className="div-tf">
      <label className={"label"}>{text}</label>
      <TextField
        InputProps={props}
        required={required}
        variant="outlined"
        defaultValue={defaultValue}
      ></TextField>
    </div>
  );
};
