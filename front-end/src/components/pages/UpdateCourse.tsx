import React from "react";
import { CourseForm } from "../course_form/CourseForm";

const UpdateCourse: React.FC = () => {
  return <CourseForm headerName="Изменение курса"></CourseForm>;
};

export default UpdateCourse;
