export type StatusType = "ACTIVE" | "BLOCKED" 
export type RoleType = "EMPLOYEE" | "PROFESSOR" | "STUDENT"

export const statusRender = (role: StatusType) => {
    switch (role) {
        case "ACTIVE":
            return 'Активен';
        default:
            return 'Заблокирован';
    }
}

export const roleRender = (role: RoleType) => {
    switch (role) {
        case "EMPLOYEE":
            return 'Сотрудник униерситета';
        case "PROFESSOR":
            return 'Преподаватель';
        default:
            return 'Студент';
    }
}