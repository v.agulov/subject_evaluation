import { StatusType, RoleType } from './Types'

export interface User {
    id: number
    email: string;
    firstName: string;
    lastName: string;
    patronymic: string;
    role: RoleType;
    status: StatusType;
    group?: number;
} 

export function createUser(
    id: number,
    email: string,
    firstName: string,
    lastName: string,
    patronymic: string,
    role: RoleType,
    status: StatusType,
    group?: number
  ): User {
    return { id, email, firstName, lastName, patronymic, role, status, group };
  }