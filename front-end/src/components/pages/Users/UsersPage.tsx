import React, { useState, useEffect } from "react";
import { User, createUser } from './User'
import { UsersTable } from './UsersTable'
import { RoleType, StatusType, statusRender, roleRender } from './Types'
import { SearchBar } from './SearchBar'
import { FilterCheckbox } from './FilterCheckbox'
import { Button, FormLabel, Grid } from '@material-ui/core'
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';

const testUsers = [
  createUser(1, "a.sergeenko@g.nsu.ru", "Антон", "Сергеенко", "Матвеевич", "STUDENT", "ACTIVE", 19220),
  createUser(2, "m.shpak@g.nsu.ru", "Мария", "Шпак", "Владимировна", "PROFESSOR", "ACTIVE", undefined),
  createUser(3, "v.zauhabkin@g.nsu.ru", "Евгений", "Заухабкин", "Олегович", "STUDENT", "BLOCKED", 17700),
  createUser(4, "j.nesterovich@g.nsu.ru", "Юлия", "Нестерович", "Андреевна", "PROFESSOR", "BLOCKED", undefined),
  createUser(5, "a.petrov@g.nsu.ru", "Александр", "Петров", "Артемович", "STUDENT", "ACTIVE", 16707),
  createUser(6, "s.velgelman@g.nsu.ru", "Станислав", "Вельгельман", "Ильич", "EMPLOYEE", "BLOCKED", undefined),
  createUser(7, "o.kim@g.nsu.ru", "Ольга", "Ким", "Алексеевна", "STUDENT", "ACTIVE", 12345),
  createUser(8, "a.peresvetov@g.nsu.ru", "Андрей", "Пересветов", "Константинович", "EMPLOYEE", "ACTIVE", undefined),
];

const UsersPage: React.FC = () => {
  const [activeFilter, setActive] = useState<boolean>(true)
  const [blockedFilter, setBlocked] = useState<boolean>(true)
  const [studentFilter, setStudent] = useState<boolean>(true)
  const [professorFilter, setProfessor] = useState<boolean>(true)
  const [employeeFilter, setEmployee] = useState<boolean>(true)
  const [searchText, setSearchText] = useState<string>('')
  const [currentUsers, setUsers] = useState<User[]>(testUsers)

  useEffect(() => {
    filter()
  }, [activeFilter, blockedFilter, studentFilter, professorFilter, employeeFilter, searchText])

  const activeFilterHandler = () => setActive(activeFilter => !activeFilter)
  const blockedFilterHandler = () => setBlocked(blockedFilter => !blockedFilter)
  const studentFilterHandler = () => setStudent(prev => !studentFilter)
  const professorFilterHandler = () => setProfessor(prev => !professorFilter)
  const employeeFilterHandler = () => setEmployee(prev => !employeeFilter)

  const searchChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value)
  }

  const keyPressedHandler = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      filter()
    }
  }

  const matches = (u: User, str: string): boolean => {
    return ((u.firstName.toLowerCase().includes(str.toLowerCase())) ||
      (u.lastName.toLowerCase().includes(str.toLowerCase())) ||
      (u.patronymic.toLowerCase().includes(str.toLowerCase())) ||
      (u.email.toLowerCase().includes(str.toLowerCase())))
  }

  const filter = () => {
    setUsers(prev => testUsers.filter((u: User) => {
      if (((studentFilter && u.role === "STUDENT") || (professorFilter && u.role === "PROFESSOR") || (employeeFilter && u.role === "EMPLOYEE")) &&
        ((activeFilter && u.status === "ACTIVE") || (blockedFilter && u.status === "BLOCKED")) && (matches(u, searchText))) {
        return u
      }
      else return null
    }))
  }



  const FilterBlock: React.FC = () => {
    return (
      <>
        <FormControl component="fieldset" >
          <FormLabel component="legend">Роли</FormLabel>
          <FormGroup>
            <FilterCheckbox label="Студент" color="primary" checked={studentFilter} onChange={studentFilterHandler} name="student" />
            <FilterCheckbox label="Преподаватель" color="primary" checked={professorFilter} onChange={professorFilterHandler} name="professor" />
            <FilterCheckbox label="Сотрудник университета" color="primary" checked={employeeFilter} onChange={employeeFilterHandler} name="employee" />
          </FormGroup>
        </FormControl>
        <FormControl component="fieldset" >
          <FormLabel component="legend">Статус</FormLabel>
          <FormGroup>
            <FilterCheckbox label="Активен" color="secondary" checked={activeFilter} onChange={activeFilterHandler} name="active" />
            <FilterCheckbox label="Заблокирован" color="secondary" checked={blockedFilter} onChange={blockedFilterHandler} name="blocked" />
          </FormGroup>
        </FormControl>
      </>)
  }

  return (
    <Grid container>
      <Grid item xs={9}>
        <UsersTable users={currentUsers} />
      </Grid>
      <Grid item xs={3}>
        <SearchBar text={searchText} placeholder="Поиск пользователя по ФИО или e-mail" value={searchText} onChange={searchChangeHandler} onEnter={keyPressedHandler} />
        <FilterBlock />
      </Grid>
    </Grid>
  );
};

export default UsersPage