import React, {useEffect, useState} from "react";
import { Header } from "../course_form/Header";
import SimpleTabs from "../view_course/TabPanel";
import {getToken} from "../Auth/TokenProvider";
import Api from "../../api/Api";
import {toAuthorizationHeaderValue} from "../Utils";
import {CourseResponseDto} from "../../api/RestApi";

export enum about {
    Description,
    Polls,
    FeedBack,
}

interface ViewCourseProps {
    about: number;
    courseId: number;
}

export default function ViewCourse(props: ViewCourseProps) {

  const [course, setCourse] = useState<CourseResponseDto>();

  useEffect(() => {
    getToken().then(accessToken =>
      Api.getCourse(props.courseId, {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
        .then((response) => {
          setCourse(response.data)
        })
    );
  },);

  let courseName: string = 'Default name';
  if (course?.name !== undefined)
    courseName = course?.name;

    return (
        <div className="main-container">
            <Header headerName={courseName} />
            <SimpleTabs tabIndex={props.about} courseId={props.courseId}/>
        </div>
    );
}