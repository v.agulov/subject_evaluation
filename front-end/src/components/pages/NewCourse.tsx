import React from "react";
import { CourseForm } from "../course_form/CourseForm";

const NewCourse: React.FC = () => {
  return <CourseForm headerName="Создание курса"></CourseForm>;
};

export default NewCourse;
